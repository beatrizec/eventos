<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Under_Event extends Model
{
    
     protected $fillable = [
        'date', 'hour', 'max_people', 'description'
    ];	
    
}
