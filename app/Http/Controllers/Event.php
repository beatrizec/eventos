<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class Event extends Controller
{
     public function store(Request $request)
    {
       $this->validate($request, [
       	'name' => 'required'
       ], [
        'description' => 'required'
       ], [
        'name.required' => 'Es necesario ingresar un nombre para el Evento'
       ], [
        'description.required' => 'Es necesario ingresar una descripción para el Evento'
       ]);
       
       //creamos la categoría 
       Event::create($request->all()); 
       // $request->all() sólo tiene en nombre de la categoría
    }

    public function update(Request $request){

        $this->validate($request, [
        'name' => 'required'
       ], [
        'name.required' => 'Es necesario ingresar un nombre para el Evento'
       ], [
        'description.required' => 'Es necesario ingresar una descripción para el Evento'
       ]

       );
       
       //buscamos el evento
        $event_id = $request->input('event_id'); 
        $event = Event::find($event_id);
        $event->name = $request->input('name');
        $event->description = $request->input('description');
        $event->save(); //guardamos los cambios
    }

    public function delete($id){
        
        Event::find($id)->delete();
        return back(); 
    }
}
